<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        db::table('employees')->insert([
            'nama' => 'Andri Setiawan',
            'jeniskelamin' => 'cowok',
            'notelpon' => '123456789098',
        ]);
    }
}
